package ru.dartit.pcab.display_params.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author a.trotsenko
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DisplayParam {
    private Integer id;
    private String name;
    private String value;
    private String unit;
    private String[] units;
}
