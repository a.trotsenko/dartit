package ru.dartit.pcab.display_params.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author a.trotsenko
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UnitType {
    private String name;
}
