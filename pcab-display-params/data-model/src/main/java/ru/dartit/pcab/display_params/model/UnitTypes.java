package ru.dartit.pcab.display_params.model;

import java.math.BigDecimal;

/**
 * @author a.trotsenko
 */
public interface UnitTypes {
    UnitType STRING = new UnitType(String.class.getName());
    UnitType INTEGER = new UnitType(Integer.class.getName());
    UnitType BOOLEAN = new UnitType(Boolean.class.getName());
    UnitType BIG_DECIMAL = new UnitType(BigDecimal.class.getName());
}
