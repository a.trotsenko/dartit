import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.google.gson.GsonBuilder;
import org.jboss.resteasy.core.Dispatcher;
import org.jboss.resteasy.core.SynchronousDispatcher;
import org.jboss.resteasy.core.SynchronousExecutionContext;
import org.jboss.resteasy.mock.MockDispatcherFactory;
import org.jboss.resteasy.mock.MockHttpRequest;
import org.jboss.resteasy.mock.MockHttpResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import ru.dartit.pcab.display_params.model.DisplayParam;
import ru.dartit.pcab.display_params.rest.CommonExceptionMapper;
import ru.dartit.pcab.display_params.rest.RestApplication;
import ru.dartit.pcab.display_params.rest.data.DataProvider;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URISyntaxException;
import java.util.Collection;

import static org.mockito.Mockito.when;

/**
 * @author a.trotsenko
 */
@RunWith(MockitoJUnitRunner.class)
public class RestServiceTest {
    private static Dispatcher dispatcher;

    private TestData testData;
    @Mock DataProvider dataProvider;
    @InjectMocks RestApplication restApplication;

    @Before
    public void setup() {
        testData = new TestData();

        //mocking
        when(dataProvider.getParams()).thenReturn(testData.getGetParamsData());
        when(dataProvider.update(testData.getTestListToUpdate())).thenReturn(testData.getUpdatedData());

        when(dataProvider.update(testData.getListToUpdateWithInvalidId())).thenThrow(new IllegalArgumentException(TestData.INVALID_ID_ERROR_MESSAGE));
        when((dataProvider.getUnits())).thenReturn(testData.getUnits());

        dispatcher = MockDispatcherFactory.createDispatcher();
        dispatcher.getRegistry().addSingletonResource(restApplication);
        dispatcher.getProviderFactory().registerProvider(JacksonJsonProvider.class);
        dispatcher.getProviderFactory().registerProvider(CommonExceptionMapper.class);
    }

    @Test
    public void testGetParams() throws URISyntaxException {
        MockHttpResponse response = sendGetRequest("/pcab-display-params", "");
        assertResponse(response, Response.Status.OK.getStatusCode(), TestData.EXPECTED_GET_PARAMS_JSON_RESPONSE);
    }

    @Test
    public void testUpdate() throws URISyntaxException {
        MockHttpResponse response = sendPostRequest("/pcab-display-params", createRequestJSON(testData.getTestListToUpdate()));
        assertResponse(response, Response.Status.OK.getStatusCode(), TestData.EXPECTED_UPDATE_JSON_RESPONSE);
    }

    /**
     * Check wrong format of request data
     */
    @Test
    public void testFailJsonMappingConversion() throws URISyntaxException {
        MockHttpResponse response = sendPostRequest("/pcab-display-params", "[\"a\": 1]");
        assertResponse(response, Response.Status.BAD_REQUEST.getStatusCode(), CommonExceptionMapper.UNKNOWN_REQUEST_DATA_FORMAT);
    }

    @Test
    public void testUpdateWithInvalidId() throws URISyntaxException {
        MockHttpResponse response = sendPostRequest("/pcab-display-params", createRequestJSON(testData.getListToUpdateWithInvalidId()));
        assertResponse(response, Response.Status.BAD_REQUEST.getStatusCode(), TestData.INVALID_ID_ERROR_MESSAGE);
    }

    @Test
    public void testGetUnits() throws URISyntaxException {
        MockHttpResponse response = sendGetRequest("/pcab-display-params/get_units", "");
        assertResponse(response, Response.Status.OK.getStatusCode(), TestData.EXPECTED_GET_UNITS_JSON_RESPONSE);
    }

    private void assertResponse(MockHttpResponse response, int expectedStatusCode, String expectedContent) {
        Assert.assertEquals(expectedStatusCode, response.getStatus());
        Assert.assertEquals(expectedContent, response.getContentAsString());
    }

    private String createRequestJSON(Collection<DisplayParam> params) {
        return new GsonBuilder().setPrettyPrinting().create().toJson(params);
    }

    private MockHttpResponse sendGetRequest(String path, String requestBody) throws URISyntaxException {
        return sendAsyncRequest(MockHttpRequest.get(path), path, requestBody);
    }
    private MockHttpResponse sendPostRequest(String path, String requestBody) throws URISyntaxException {
        return sendAsyncRequest(MockHttpRequest.post(path), path, requestBody);
    }
    private MockHttpResponse sendAsyncRequest(MockHttpRequest request, String path, String requestBody) throws URISyntaxException {
        request.accept(MediaType.APPLICATION_JSON);
        request.contentType(MediaType.APPLICATION_JSON_TYPE);
        request.content(requestBody.getBytes());

        MockHttpResponse response = new MockHttpResponse();
        SynchronousExecutionContext synchronousExecutionContext = new SynchronousExecutionContext((SynchronousDispatcher) dispatcher, request, response);
        request.setAsynchronousContext(synchronousExecutionContext);
        return sendHttpRequest(request, response);
    }

    private MockHttpResponse sendHttpRequest(MockHttpRequest request, MockHttpResponse response) {
        dispatcher.invoke(request, response);
        return response;
    }
}
