import ru.dartit.pcab.display_params.model.DisplayParam;
import ru.dartit.pcab.display_params.model.Unit;
import ru.dartit.pcab.display_params.model.UnitTypes;

import java.util.*;

/**
 * @author a.trotsenko
 */
public class TestData {
    private final Map<Integer, DisplayParam> data;
    private final Collection<Unit> units;

    private final Collection<DisplayParam> getParamsData;
    private final Collection<DisplayParam> updatedData;
    private final Collection<DisplayParam> testListToUpdate;
    private final Collection<DisplayParam> listToUpdateWithInvalidId;

    private static final Integer ID_TO_UPDATE = 5;
    private static final String UPDATED_VALUE = "false";
    private static final String UPDATED_UNIT = "other boolean unit";
    public static final String EXPECTED_GET_PARAMS_JSON_RESPONSE = "[{\"id\":4,\"name\":\"Период хранения заказанных документов в ЛК\",\"value\":\"7\",\"unit\":\"День\",\"units\":[\"День\",\"Месяц\"]},{\"id\":5,\"name\":\"On/off param\",\"value\":\"true\",\"unit\":\"boolean unit\",\"units\":[\"other boolean unit\"]}]";
    public static final String EXPECTED_UPDATE_JSON_RESPONSE = "[{\"id\":4,\"name\":\"Период хранения заказанных документов в ЛК\",\"value\":\"7\",\"unit\":\"День\",\"units\":[\"День\",\"Месяц\"]},{\"id\":5,\"name\":\"On/off param\",\"value\":\"false\",\"unit\":\"other boolean unit\",\"units\":[\"other boolean unit\"]}]";
    public static final String EXPECTED_GET_UNITS_JSON_RESPONSE = "[{\"name\":\"Пример числовой ед. изм.\",\"type\":{\"name\":\"java.lang.Integer\"}},{\"name\":\"Пример boolean ед. изм.\",\"type\":{\"name\":\"java.lang.Boolean\"}},{\"name\":\"Пример строковой ед. изм.\",\"type\":{\"name\":\"java.lang.String\"}}]";
    public static final String INVALID_ID_ERROR_MESSAGE = "Not found param with id";

    {
        data = initData();
        getParamsData = initGetParamsData(data);
        testListToUpdate = initListToUpdate(ID_TO_UPDATE, UPDATED_VALUE, UPDATED_UNIT);
        updatedData = initUpdatedData(data);
        listToUpdateWithInvalidId = initListToUpdate(-1, null, null);
        units = initUnits();
    }

    private Collection<Unit> initUnits() {
        Unit integerUnit = new Unit("Пример числовой ед. изм.", UnitTypes.INTEGER);
        Unit booleanUnit = new Unit("Пример boolean ед. изм.", UnitTypes.BOOLEAN);
        Unit stringUnit = new Unit("Пример строковой ед. изм.", UnitTypes.STRING);
        return Arrays.asList(integerUnit, booleanUnit, stringUnit);
    }

    private Map<Integer, DisplayParam> initData() {
        Map<Integer, DisplayParam> data = new LinkedHashMap<>();
        data.put(4, new DisplayParam(4,
                "Период хранения заказанных документов в ЛК",
                "7",
                "День",
                new String[]{"День", "Месяц"}));
        data.put(5, new DisplayParam(5,
                "On/off param",
                "true",
                "boolean unit",
                new String[]{"other boolean unit"}));
        return data;
    }

    private Collection<DisplayParam> initUpdatedData(Map<Integer, DisplayParam> data) {
        Collection<DisplayParam> list = new ArrayList<>();
        for (Integer id : data.keySet()) {
            DisplayParam param = data.get(id);
            //replace edited object to avoid changing getParamsData
            if (id.equals(ID_TO_UPDATE)) {
                param = new DisplayParam(param.getId(),
                        param.getName(),
                        UPDATED_VALUE,
                        UPDATED_UNIT,
                        param.getUnits());
            }
            list.add(param);
        }
        return list;
    }

    private Collection<DisplayParam> initGetParamsData(Map<Integer, DisplayParam> data) {
        return data.values();
    }

    public Collection<DisplayParam> initListToUpdate(Integer id, String value, String unit) {
        DisplayParam param = new DisplayParam();
        param.setId(id);
        param.setValue(value);
        param.setUnit(unit);
        Collection<DisplayParam> list = new ArrayList<>();
        list.add(param);
        return list;
    }

    public Collection<DisplayParam> getGetParamsData() {
        return getParamsData;
    }

    public Collection<DisplayParam> getTestListToUpdate() {
        return testListToUpdate;
    }

    public Collection<DisplayParam> getUpdatedData() {
        return updatedData;
    }

    public Collection<DisplayParam> getListToUpdateWithInvalidId() {
        return listToUpdateWithInvalidId;
    }

    public Collection<Unit> getUnits() {
        return units;
    }
}
