package ru.dartit.pcab.display_params.rest;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import ru.dartit.pcab.display_params.model.DisplayParam;
import ru.dartit.pcab.display_params.model.Unit;
import ru.dartit.pcab.display_params.rest.data.DataProvider;

import javax.ws.rs.*;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * @author a.trotsenko
 */
@Path("/pcab-display-params")
@Component(immediate = true, service = Application.class, property = {"jaxrs.application=true"})
public class RestApplication extends Application {
	private static final String CONTENT_TYPE = MediaType.APPLICATION_JSON + "; charset=UTF-8";

	@Reference
	private DataProvider dataProvider;

	@GET
	@Produces(CONTENT_TYPE)
	public Collection<DisplayParam> getParams(){
		return dataProvider.getParams();
	}

	@Path("/get_units")
	@GET
	@Produces(CONTENT_TYPE)
	public Collection<Unit> getUnits(){
		return dataProvider.getUnits();
	}

	@POST
	@Consumes(CONTENT_TYPE)
	@Produces(CONTENT_TYPE)
	public Collection<DisplayParam> update(Collection<DisplayParam> params) {
		return dataProvider.update(params);
	}

	/**
	 * This is a normal way according to documentation
	 */
	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> s = new HashSet<>();
		s.add(RestApplication.class);
		s.add(CommonExceptionMapper.class);
		s.add(JacksonJsonProvider.class);
		return s;
	}

	public Set<Object> getSingletons() {
		return Collections.singleton(this);
	}
}