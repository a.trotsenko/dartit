package ru.dartit.pcab.display_params.rest.data;

import ru.dartit.pcab.display_params.model.DisplayParam;
import ru.dartit.pcab.display_params.model.Unit;

import java.util.Collection;

/**
 * @author a.trotsenko
 */
public interface DataProvider {
    Collection<DisplayParam> getParams();

    /**
     * @param params to update
     * @return all params
     */
    Collection<DisplayParam> update(Collection<DisplayParam> params);

    Collection<Unit> getUnits();
}
