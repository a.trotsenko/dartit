package ru.dartit.pcab.display_params.rest;

import com.fasterxml.jackson.databind.exc.MismatchedInputException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author a.trotsenko
 */
@Provider
public class CommonExceptionMapper implements ExceptionMapper<Throwable> {

    public static final String UNKNOWN_REQUEST_DATA_FORMAT = "Не удалось распознать формат данных запроса";

    @Override
    public Response toResponse(Throwable t) {
        String message = t.getMessage();
        if (t instanceof MismatchedInputException) {
            message = UNKNOWN_REQUEST_DATA_FORMAT;
        }
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(message)
                .build();
    }
}