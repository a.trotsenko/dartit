package ru.dartit.pcab.display_params.rest.data;

import org.osgi.service.component.annotations.Component;
import ru.dartit.pcab.display_params.model.DisplayParam;
import ru.dartit.pcab.display_params.model.Unit;
import ru.dartit.pcab.display_params.model.UnitType;
import ru.dartit.pcab.display_params.model.UnitTypes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author a.trotsenko
 */
@Component(service = DataProvider.class)
public class SampleDataProvider implements DataProvider {

    private Map<Integer, DisplayParam> map;

    public SampleDataProvider() {
        map = new HashMap<>();

        addParamToMap(map, new DisplayParam(1, "Количество счетов на главной странице", "12", "Строка", new String[]{"Строка"}));
        addParamToMap(map, new DisplayParam(2, "Количество платежей на главной странице", "5", "Строка", new String[]{"Строка", "Страница"}));
        addParamToMap(map, new DisplayParam(3, "Глубина периода для поиска данных (платежи, счета, события и т.п.)", "3", "Месяц", new String[]{"Неделя", "Месяц", "Год"}));
        addParamToMap(map, new DisplayParam(4, "Период хранения заказанных документов в ЛК", "7", "День", new String[]{"Час", "День", "Месяц", "Год"}));
        addParamToMap(map, new DisplayParam(5, "Включить биореактор", "true", "Включено", new String[]{"Включено"}));
        addParamToMap(map, new DisplayParam(6, "Текстовый параметр", "some text", "Текст", new String[]{"Текст"}));
        addParamToMap(map, new DisplayParam(7, "Параметр с неподдерживаемым типом единицы измерения", "some value 1", "Пример неподдерживаемой ед. изм.", new String[]{"Пример неподдерживаемой ед. изм."}));
        addParamToMap(map, new DisplayParam(8, "Параметр с несуществующим типом единицей измерения", "some value 2", "Пример ед. изм. с несуществующим типом", new String[]{"Пример ед. изм. с несуществующим типом"}));
        addParamToMap(map, new DisplayParam(9, "Параметр с пустым списком единиц", "some value 3", "", new String[]{}));
    }

    private void addParamToMap(Map<Integer, DisplayParam> map, DisplayParam param) {
        map.put(param.getId(), param);
    }

    @Override
    public Collection<DisplayParam> getParams() {
        return map.values();
    }

    @Override
    public Collection<DisplayParam> update(Collection<DisplayParam> params) {
        params.forEach(p -> update(p.getId(), p.getValue(), p.getUnit()));
        return getParams();
    }

    private void update(Integer id, String value, String unit) {
        DisplayParam param = map.get(id);
        if (param == null) {
            throw new IllegalArgumentException(String.format("Не найден параметр с идентификатором = [%d]", id));
        }
        param.setValue(value);
        param.setUnit(unit);
    }

    @Override
    public Collection<Unit> getUnits() {
        UnitType stringType = UnitTypes.STRING;
        UnitType integerType = UnitTypes.INTEGER;
        UnitType booleanType = UnitTypes.BOOLEAN;

        Unit stringUnit = new Unit("Строка", integerType);
        Unit dayUnit = new Unit("День", integerType);
        Unit monthUnit = new Unit("Месяц", integerType);
        Unit yearUnit = new Unit("Год", integerType);
        Unit hourUnit = new Unit("Час", integerType);
        Unit weekUnit = new Unit("Неделя", integerType);
        Unit pageUnit = new Unit("Страница", integerType);
        Unit onOffUnit = new Unit("Включено", booleanType);
        Unit textUnit = new Unit("Текст", stringType);
        Unit bigDecimalUnit = new Unit("Пример неподдерживаемой ед. изм.", UnitTypes.BIG_DECIMAL);

        Collection<Unit> units = new ArrayList<>();
        units.add(stringUnit);
        units.add(dayUnit);
        units.add(monthUnit);
        units.add(yearUnit);
        units.add(hourUnit);
        units.add(weekUnit);
        units.add(pageUnit);
        units.add(onOffUnit);
        units.add(textUnit);
        units.add(bigDecimalUnit);

        return units;
    }
}
