package ru.dartit;

import com.google.gson.GsonBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;
import ru.dartit.pcab.display_params.model.DisplayParam;
import ru.dartit.pcab.display_params.model.Unit;
import ru.dartit.pcab.display_params.model.UnitType;
import ru.dartit.pcab.display_params.rest.client.DisplayParamsRestClient;
import ru.dartit.pcab.display_params.rest.client.DisplayParamsRestClientImpl;
import ru.dartit.pcab.display_params.rest.client.RestClient;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

@RunWith(MockitoJUnitRunner.class)
public class RestClientTest {
    private final String TEST_URL = "http://test";

    private MockRestServiceServer mockServer;

    RestClient restClient = new RestClient();
    @Mock DisplayParamsRestClient displayParamsRestClient = new DisplayParamsRestClientImpl();

    @Before
    public void setUp() {
        when(displayParamsRestClient.getRestClient()).thenReturn(restClient);
        RestTemplate restTemplate = displayParamsRestClient.getRestClient().getRestTemplate();
        mockServer = MockRestServiceServer.createServer(restTemplate);
    }

    @Test
    public void testSuccessJsonMappingConversion() {
        UnitType unitType1 = new UnitType("type1");
        UnitType unitType2 = new UnitType("type2");

        Unit unit1 = new Unit("Ед. изм. 1", unitType1);
        Unit unit2 = new Unit("Ед. изм. 2", unitType2);

        DisplayParam expectedParam = new DisplayParam();
        expectedParam.setId(1);
        expectedParam.setName("Тестовый параметр");
        expectedParam.setValue("11");
        expectedParam.setUnit(unit1.getName());
        expectedParam.setUnits(new String[]{unit1.getName(), unit2.getName()});

        mockServer.expect(requestTo(TEST_URL))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess(toJson(expectedParam), MediaType.APPLICATION_JSON));

        DisplayParam resultParam = displayParamsRestClient.getRestClient().getRestTemplate().getForObject(TEST_URL,
                DisplayParam.class);

        Assert.assertEquals(expectedParam, resultParam);

        mockServer.verify();
    }

    @Test
    public void testFailJsonMappingConversion() {
        mockServer.expect(requestTo(TEST_URL))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withSuccess("[{\"id\": \"1\"}]", MediaType.APPLICATION_JSON));

        boolean wasError = false;
        try {
            displayParamsRestClient.getRestClient().getRestTemplate().getForObject(TEST_URL, DisplayParam.class);
        } catch (Throwable t) {
            wasError = true;
            Assert.assertEquals(HttpMessageNotReadableException.class, t.getClass());
        }
        Assert.assertTrue(wasError);
    }

    private String toJson(Object o) {
        return new GsonBuilder().setPrettyPrinting().create().toJson(o);
    }
}