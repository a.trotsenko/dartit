package ru.dartit.pcab.display_params.rest.client.exception;

import java.io.IOException;

/**
 * @author a.trotsenko
 */
public class RestException extends IOException {
    private int code;

    public RestException(int code, String body) {
        super(body + " (" + code + ")");
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
