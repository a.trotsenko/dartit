package ru.dartit.pcab.display_params.constants;

/**
 * @author a.trotsenko
 */
public enum PreferenceName {
    ENDPOINT, GET_PARAMS_PATH, UPDATE_PATH, GET_UNITS_PATH,
    CONNECTION_REQUEST_TIMEOUT, CONNECT_TIMEOUT, READ_TIMEOUT
}
