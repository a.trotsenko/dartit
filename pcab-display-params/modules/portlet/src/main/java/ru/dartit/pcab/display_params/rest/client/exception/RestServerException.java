package ru.dartit.pcab.display_params.rest.client.exception;

/**
 * @author a.trotsenko
 */
public class RestServerException extends RestException {
    public RestServerException(int code, String body) {
        super(code, body);
    }
}
