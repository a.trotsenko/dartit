package ru.dartit.pcab.display_params.rest.client.exception;

/**
 * @author a.trotsenko
 */
public class RestClientException extends RestException {
    public RestClientException(int code, String body) {
        super(code, body);
    }
}
