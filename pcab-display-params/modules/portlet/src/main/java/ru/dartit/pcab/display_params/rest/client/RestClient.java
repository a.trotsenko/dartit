package ru.dartit.pcab.display_params.rest.client;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import ru.dartit.pcab.display_params.model.DisplayParam;
import ru.dartit.pcab.display_params.rest.client.exception.RestTemplateResponseErrorHandler;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author a.trotsenko
 */
public class RestClient {
    private RestTemplate restTemplate;
    private String endpoint;

    public RestClient() {
        HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        restTemplate = new RestTemplate(httpRequestFactory);
        restTemplate.setErrorHandler(new RestTemplateResponseErrorHandler());
        restTemplate.setMessageConverters(Arrays.asList(new MappingJackson2HttpMessageConverter()));
    }

    private void validateState(String path) {
        if (this.endpoint == null) {
            throw new IllegalStateException("ENDPOINT must be not null");
        }
        if (StringUtils.isEmpty(path)) {
            throw new IllegalArgumentException("path must be not empty");
        }
    }

    protected final <T> T getList(String path, ParameterizedTypeReference<T> typeReference) throws RestClientException {
        validateState(path);
        return getList(HttpMethod.GET, path, typeReference);
    }

    public final Collection<DisplayParam> updateParams(String path, Collection<DisplayParam> params) throws RestClientException {
        validateState(path);
        HttpEntity<Collection<DisplayParam>> request = new HttpEntity<>(params);
        return restTemplate.postForObject(endpoint + path, request, Collection.class);
    }

    private final <T> T getList(HttpMethod method, String path, ParameterizedTypeReference<T> typeReference) throws RestClientException {
        return restTemplate.exchange(endpoint + path, method, null, typeReference).getBody();
    }

    protected void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    protected void setConnectionRequestTimeout(int seconds) {
        ((HttpComponentsClientHttpRequestFactory) restTemplate.getRequestFactory()).setConnectionRequestTimeout(seconds * 1000);
    }

    protected void setConnectTimeout(int seconds) {
        ((HttpComponentsClientHttpRequestFactory) restTemplate.getRequestFactory()).setConnectTimeout(seconds * 1000);
    }

    protected void setReadTimeout(int seconds) {
        ((HttpComponentsClientHttpRequestFactory) restTemplate.getRequestFactory()).setReadTimeout(seconds * 1000);
    }

    public RestTemplate getRestTemplate() {
        return restTemplate;
    }
}
