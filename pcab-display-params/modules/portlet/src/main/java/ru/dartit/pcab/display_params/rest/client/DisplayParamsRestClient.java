package ru.dartit.pcab.display_params.rest.client;

import ru.dartit.pcab.display_params.model.DisplayParam;
import ru.dartit.pcab.display_params.model.Unit;

import java.util.Collection;

/**
 * @author a.trotsenko
 */
public interface DisplayParamsRestClient {
    void setEndpoint(String endpoint);
    void setGetParamsPath(String path);
    void setUpdatePath(String path);
    void setGetUnitsPath(String path);

    void setConnectionRequestTimeout(int seconds);
    void setConnectTimeout(int seconds);
    void setReadTimeout(int seconds);

    RestClient getRestClient();

    Collection<DisplayParam> getParams();

    /**
     * @param params to update
     * @return all params
     */
    Collection<DisplayParam> update(Collection<DisplayParam> params);

    Collection<Unit> getUnits();
}
