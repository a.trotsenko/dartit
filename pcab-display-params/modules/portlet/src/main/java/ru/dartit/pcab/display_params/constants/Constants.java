package ru.dartit.pcab.display_params.constants;

/**
 * @author a.trotsenko
 */
public class Constants {
    public static final String PORTLET_NAME = "pcab.display.params.portlet";

    public static final String DEFAULT_ENDPOINT = "http://localhost:8080/o/rest";
    public static final String DEFAULT_GET_PARAMS_PATH = "/pcab-display-params";
    public static final String DEFAULT_UPDATE_PATH = "/pcab-display-params";
    public static final String DEFAULT_GET_UNITS_PATH = "/pcab-display-params/get_units";

    //seconds
    public static final Integer DEFAULT_CONNECTION_REQUEST_TIMEOUT = 3;
    public static final Integer DEFAULT_CONNECTION_TIMEOUT = 2;
    public static final Integer DEFAULT_READ_TIMEOUT = 1;
}
