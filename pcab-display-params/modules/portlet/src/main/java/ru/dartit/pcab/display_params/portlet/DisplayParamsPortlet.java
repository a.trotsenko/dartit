package ru.dartit.pcab.display_params.portlet;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.servlet.SessionErrors;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.springframework.util.StringUtils;
import ru.dartit.pcab.display_params.constants.Constants;
import ru.dartit.pcab.display_params.constants.PreferenceName;
import ru.dartit.pcab.display_params.constants.RequestAttributeKeys;
import ru.dartit.pcab.display_params.model.DisplayParam;
import ru.dartit.pcab.display_params.model.Unit;
import ru.dartit.pcab.display_params.rest.client.DisplayParamsRestClient;

import javax.portlet.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * @author a.trotsenko
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.dartit.pcab.display_params",
		"com.liferay.portlet.instanceable=true",
		"com.liferay.portlet.header-portlet-css=/css/styles.css",
		"javax.portlet.display-name=Private cabinet display params",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.portlet-mode=text/html;view,edit",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.init-param.edit-template=/edit.jsp",
		"javax.portlet.preferences=ENDPOINT",
		"javax.portlet.preferences=GET_PARAMS_PATH",
		"javax.portlet.preferences=UPDATE_PATH",
		"javax.portlet.preferences=GET_UNITS_PATH",
		"javax.portlet.preferences=CONNECTION_REQUEST_TIMEOUT",
		"javax.portlet.preferences=CONNECT_TIMEOUT",
		"javax.portlet.preferences=READ_TIMEOUT",
		"javax.portlet.name=" + Constants.PORTLET_NAME,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class DisplayParamsPortlet extends MVCPortlet {

    private static Log log = LogFactoryUtil.getLog(DisplayParamsPortlet.class);

    @Reference
    private DisplayParamsRestClient restClient;

	@ProcessAction(name="savePrefs")
	public void savePrefs(ActionRequest actionRequest, ActionResponse response) throws ReadOnlyException, IOException, ValidatorException {
		//get form input values
		String endpoint = actionRequest.getParameter(PreferenceName.ENDPOINT.name());
		String getParamsPath = actionRequest.getParameter(PreferenceName.GET_PARAMS_PATH.name());
		String updatePath = actionRequest.getParameter(PreferenceName.UPDATE_PATH.name());
		String getUnitsPath = actionRequest.getParameter(PreferenceName.GET_UNITS_PATH.name());

		String connectionRequestTimeout = actionRequest.getParameter(PreferenceName.CONNECTION_REQUEST_TIMEOUT.name());
		String connectionTimeout = actionRequest.getParameter(PreferenceName.CONNECT_TIMEOUT.name());
		String readTimeout = actionRequest.getParameter(PreferenceName.READ_TIMEOUT.name());

		//put to prefs
		actionRequest.getPreferences().setValue(PreferenceName.ENDPOINT.name(), endpoint);
		actionRequest.getPreferences().setValue(PreferenceName.GET_PARAMS_PATH.name(), getParamsPath);
		actionRequest.getPreferences().setValue(PreferenceName.UPDATE_PATH.name(), updatePath);
		actionRequest.getPreferences().setValue(PreferenceName.GET_UNITS_PATH.name(), getUnitsPath);

		actionRequest.getPreferences().setValue(PreferenceName.CONNECTION_REQUEST_TIMEOUT.name(), connectionRequestTimeout);
		actionRequest.getPreferences().setValue(PreferenceName.CONNECT_TIMEOUT.name(), connectionTimeout);
		actionRequest.getPreferences().setValue(PreferenceName.READ_TIMEOUT.name(), readTimeout);

		//store
		actionRequest.getPreferences().store();
	}

	@ProcessAction(name = "updateParams")
	public void updateParams(ActionRequest actionRequest, ActionResponse response) {
        Collection<DisplayParam> params = (Collection<DisplayParam>) actionRequest.getPortletSession().getAttribute(RequestAttributeKeys.PARAMS);
        Collection<DisplayParam> paramsToUpdate = new ArrayList<>();
		boolean needUpdate;
        for (DisplayParam param : params) {
        	needUpdate = false;
            String value = actionRequest.getParameter("value_" + param.getId());
            String unit = actionRequest.getParameter("unit_" + param.getId());

			if (value != null && !value.equals(param.getValue())) {
				param.setValue(value);
				needUpdate = true;
			}
			if (unit != null && !unit.equals(param.getUnit())) {
				param.setUnit(unit);
				needUpdate = true;
			}

			if (needUpdate) {
				paramsToUpdate.add(param);
			}
        }
		if (!paramsToUpdate.isEmpty()) {
			Collection<DisplayParam> data = getFromRestWithErrorHandling(actionRequest, () -> restClient.update(paramsToUpdate));
			actionRequest.setAttribute(RequestAttributeKeys.PARAMS, data);
		}
	}


	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		renderRequest.setAttribute(RequestAttributeKeys.PREFS_VALID, validatePrefs(renderRequest.getPreferences()));

		initRestClient(renderRequest.getPreferences());

		//load params

		if (renderRequest.getPortletSession().getAttribute(RequestAttributeKeys.PARAMS_AFTER_UPDATE) != null) {
			//load after update if it was
			//put in request
			renderRequest.getPortletSession().setAttribute(RequestAttributeKeys.PARAMS, renderRequest.getPortletSession().getAttribute(RequestAttributeKeys.PARAMS_AFTER_UPDATE));
			//remove from session
			renderRequest.getPortletSession().setAttribute(RequestAttributeKeys.PARAMS_AFTER_UPDATE, null);
		} else {
			//get from rest
			loadDataAndSaveInRequest(RequestAttributeKeys.PARAMS, () -> restClient.getParams(), renderRequest,
					params -> {
						renderRequest.getPortletSession().setAttribute(RequestAttributeKeys.PARAMS, params);
						return params;
					});
		}

		//load units
		loadDataAndSaveInRequest(RequestAttributeKeys.UNIT_TYPES, () -> restClient.getUnits(), renderRequest,
				units -> units.stream().collect(Collectors.toMap(Unit::getName, Unit::getType)));

		super.doView(renderRequest, renderResponse);
	}

	private <T> T getFromRestWithErrorHandling(PortletRequest request, Supplier<T> supplier) {
		try {
			return supplier.get();
		} catch (Throwable e) {
			log.error(e);
			SessionErrors.add(request, ExceptionUtils.getRootCause(e).getClass());
		}
		return null;
	}

	private <T, R> void loadDataAndSaveInRequest(String attrName, Supplier<T> supplier, PortletRequest portletRequest, Function<T, R> converter) {
		T data = getFromRestWithErrorHandling(portletRequest, supplier);
		Object value = data;
		if (converter != null && data != null) {
			value = converter.apply(data);
		}
		portletRequest.setAttribute(attrName, value);
	}

	private boolean validatePrefs(PortletPreferences portletPreferences) {
		return validatePref(portletPreferences, PreferenceName.ENDPOINT)
				&& validatePref(portletPreferences, PreferenceName.GET_PARAMS_PATH)
				&& validatePref((portletPreferences), PreferenceName.UPDATE_PATH);
	}

	private boolean validatePref(PortletPreferences portletPreferences, PreferenceName preferenceName) {
		return !StringUtils.isEmpty(portletPreferences.getValue(preferenceName.name(), ""));
	}

	private String getPrefValue(PortletPreferences portletPreferences, PreferenceName preferenceName, String defaultValue) {
		String value = portletPreferences.getValue(preferenceName.name(), "");
		if (StringUtils.isEmpty(value)) {
			value = defaultValue;
		}
		return value;
	}

	private void initRestClient(PortletPreferences portletPreferences) {
		restClient.setEndpoint(getPrefValue(portletPreferences, PreferenceName.ENDPOINT, Constants.DEFAULT_ENDPOINT));
		restClient.setGetParamsPath(getPrefValue(portletPreferences, PreferenceName.GET_PARAMS_PATH, Constants.DEFAULT_GET_PARAMS_PATH));
		restClient.setUpdatePath(getPrefValue(portletPreferences, PreferenceName.UPDATE_PATH, Constants.DEFAULT_UPDATE_PATH));
		restClient.setGetUnitsPath(getPrefValue(portletPreferences, PreferenceName.GET_UNITS_PATH, Constants.DEFAULT_GET_UNITS_PATH));

		restClient.setConnectionRequestTimeout(Integer.valueOf(getPrefValue(portletPreferences, PreferenceName.CONNECTION_REQUEST_TIMEOUT, Constants.DEFAULT_CONNECTION_REQUEST_TIMEOUT.toString())));
		restClient.setConnectTimeout(Integer.valueOf(getPrefValue(portletPreferences, PreferenceName.CONNECT_TIMEOUT, Constants.DEFAULT_CONNECTION_TIMEOUT.toString())));
		restClient.setReadTimeout(Integer.valueOf(getPrefValue(portletPreferences, PreferenceName.READ_TIMEOUT, Constants.DEFAULT_READ_TIMEOUT.toString())));
	}
}