package ru.dartit.pcab.display_params.rest.client.exception;

/**
 * @author a.trotsenko
 */
public class ResourceNotFountException extends RestException {

    public ResourceNotFountException(int code, String body) {
        super(code, body);
    }
}
