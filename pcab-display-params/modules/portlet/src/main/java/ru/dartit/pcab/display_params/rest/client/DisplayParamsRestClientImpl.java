package ru.dartit.pcab.display_params.rest.client;

import org.osgi.service.component.annotations.Component;
import org.springframework.core.ParameterizedTypeReference;
import ru.dartit.pcab.display_params.model.DisplayParam;
import ru.dartit.pcab.display_params.model.Unit;

import java.util.Collection;

/**
 * @author a.trotsenko
 */
@Component(service = DisplayParamsRestClient.class)
public class DisplayParamsRestClientImpl implements DisplayParamsRestClient {
    private RestClient restClient = new RestClient();

    private String getParamsPath;
    private String updatePath;
    private String getUnitsPath;

    public Collection<DisplayParam> getParams() {
        return getRestClient().getList(getParamsPath, new ParameterizedTypeReference<Collection<DisplayParam>>(){});
    }

    @Override
    public Collection<Unit> getUnits() {
        return getRestClient().getList(getUnitsPath, new ParameterizedTypeReference<Collection<Unit>>(){});
    }

    @Override
    public Collection<DisplayParam> update(Collection<DisplayParam> params) {
        return getRestClient().updateParams(updatePath, params);
    }

    public void setEndpoint(String endpoint) {
        getRestClient().setEndpoint(endpoint);
    }

    public void setGetParamsPath(String getParamsPath) {
        this.getParamsPath = getParamsPath;
    }

    public void setUpdatePath(String updatePath) {
        this.updatePath = updatePath;
    }

    @Override
    public void setGetUnitsPath(String path) {
        this.getUnitsPath = path;
    }

    @Override
    public void setConnectionRequestTimeout(int seconds) {
        getRestClient().setConnectionRequestTimeout(seconds);
    }

    @Override
    public void setConnectTimeout(int seconds) {
        getRestClient().setConnectTimeout(seconds);
    }

    @Override
    public void setReadTimeout(int seconds) {
        getRestClient().setReadTimeout(seconds);
    }

    @Override
    public RestClient getRestClient() {
        return restClient;
    }
}
