package ru.dartit.pcab.display_params.constants;

/**
 * @author a.trotsenko
 */
public class RequestAttributeKeys {
    public static final String PARAMS = "params";
    public static final String PARAMS_AFTER_UPDATE = "params_after_update";
    public static final String UNIT_TYPES = "unit_types";
    public static final String PREFS_VALID = "prefs_valid";

    public String getUNIT_TYPES() {
        return UNIT_TYPES;
    }

    public String getPREFS_VALID() {
        return PREFS_VALID;
    }

    public String getPARAMS() {
        return PARAMS;
    }
}
