<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="ru.dartit.pcab.display_params.constants.PreferenceName" %>
<%@ page import="ru.dartit.pcab.display_params.constants.Constants" %>
<%@ page import="org.springframework.util.StringUtils" %>
<%@ include file="/init.jsp" %>

<p>
	<b><liferay-ui:message key="edit.mode.header"/></b>

	<%
		PortletPreferences pref = renderRequest.getPreferences();
		String endpoint = pref.getValue(PreferenceName.ENDPOINT.name(), "");
		String getParamsPath = pref.getValue(PreferenceName.GET_PARAMS_PATH.name(), "");
		String updatePath = pref.getValue(PreferenceName.UPDATE_PATH.name(), "");
		String getUnitsPath = pref.getValue(PreferenceName.GET_UNITS_PATH.name(), "");

		String connectionRequestTimeout = pref.getValue(PreferenceName.CONNECTION_REQUEST_TIMEOUT.name(), "");
		String connectTimeout = pref.getValue(PreferenceName.CONNECT_TIMEOUT.name(), "");
		String readTimeout = pref.getValue(PreferenceName.READ_TIMEOUT.name(), "");

		if (StringUtils.isEmpty(connectionRequestTimeout)) {
		    connectionRequestTimeout = Constants.DEFAULT_CONNECTION_REQUEST_TIMEOUT.toString();
		}
		if (StringUtils.isEmpty(connectTimeout)) {
			connectTimeout = Constants.DEFAULT_CONNECTION_TIMEOUT.toString();
		}
		if (StringUtils.isEmpty(readTimeout)) {
			readTimeout = Constants.DEFAULT_READ_TIMEOUT.toString();
		}
	%>

	<br>

	<portlet:actionURL name="savePrefs" var="savePrefsURL" />
	<aui:form action="<%=savePrefsURL%>" method="POST">
		<aui:fieldset-group markupView="lexicon">
		<aui:fieldset>
			<aui:input name="<%=PreferenceName.ENDPOINT.name()%>" value="<%= endpoint %>" required="<%= true %>" type="text"
					   placeholder="<%=Constants.DEFAULT_ENDPOINT%>" />
			<aui:input name="<%=PreferenceName.GET_PARAMS_PATH.name()%>" value="<%= getParamsPath %>" required="<%= true %>" type="text"
					   placeholder="<%=Constants.DEFAULT_GET_PARAMS_PATH%>" />
			<aui:input name="<%=PreferenceName.UPDATE_PATH.name()%>" value="<%= updatePath %>" required="<%= true %>" type="text"
					   placeholder="<%=Constants.DEFAULT_UPDATE_PATH%>" />
			<aui:input name="<%=PreferenceName.GET_UNITS_PATH.name()%>" value="<%= getUnitsPath %>" required="<%= true %>" type="text"
					   placeholder="<%=Constants.DEFAULT_GET_UNITS_PATH%>" />
		</aui:fieldset>
			<aui:fieldset>
				<aui:input name="<%=PreferenceName.CONNECTION_REQUEST_TIMEOUT.name()%>"
                           value="<%= connectionRequestTimeout %>" required="<%= true %>" type="text">
					<aui:validator name="number" />
					<aui:validator name="range">[0,60]</aui:validator>
				</aui:input>
				<aui:input name="<%=PreferenceName.CONNECT_TIMEOUT.name()%>"
						   value="<%= connectTimeout %>" required="<%= true %>" type="text">
					<aui:validator name="number" />
					<aui:validator name="range">[0,60]</aui:validator>
				</aui:input>
				<aui:input name="<%=PreferenceName.READ_TIMEOUT.name()%>"
						   value="<%= readTimeout %>" required="<%= true %>" type="text">
					<aui:validator name="number" />
					<aui:validator name="range">[0,60]</aui:validator>
				</aui:input>
			</aui:fieldset>
		</aui:fieldset-group>
		<aui:button-row>
			<aui:button type="submit" value="savePrefs" />
		</aui:button-row>
	</aui:form>

</p>