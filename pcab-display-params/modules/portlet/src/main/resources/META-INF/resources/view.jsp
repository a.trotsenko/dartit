<%@ page import="org.springframework.http.converter.HttpMessageNotReadableException" %>
<%@ page import="ru.dartit.pcab.display_params.rest.client.exception.ResourceNotFountException" %>
<%@ page import="ru.dartit.pcab.display_params.rest.client.exception.RestClientException" %>
<%@ page import="java.io.InterruptedIOException" %>
<%@ page import="java.net.ConnectException" %>
<%@ page import="java.net.UnknownHostException" %>
<%@ page import="ru.dartit.pcab.display_params.rest.client.exception.RestServerException" %>

<%@ include file="/init.jsp" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:useBean id="ReqAttrKeys" class="ru.dartit.pcab.display_params.constants.RequestAttributeKeys" scope="session"/>

	<c:if test="${!renderRequest.getAttribute(ReqAttrKeys.PREFS_VALID)}">
		<div class="portlet-msg-alert">
			<liferay-ui:message key="checkPrefs"/>
		</div>
	</c:if>

	<liferay-ui:error exception="<%=HttpMessageNotReadableException.class%>" message="json-mapping-conversion-failed"/>
	<liferay-ui:error exception="<%=ConnectException.class%>" message="connect-failed"/>
	<liferay-ui:error exception="<%=InterruptedIOException.class%>" message="connect-failed"/>
	<liferay-ui:error exception="<%=ResourceNotFountException.class%>" message="resource-not-found"/>
	<liferay-ui:error exception="<%=RestClientException.class%>" message="bad-request"/>
	<liferay-ui:error exception="<%=RestServerException.class%>" message="server-error"/>
	<liferay-ui:error exception="<%=UnknownHostException.class%>" message="unknown-host"/>

	<c:set var="BOOLEAN_UNIT_TYPE" value="<%=Boolean.class.getName()%>" />
	<c:set var="INTEGER_UNIT_TYPE" value="<%=Integer.class.getName()%>" />
	<c:set var="STRING_UNIT_TYPE" value="<%=String.class.getName()%>" />

	<c:set var="data" value="${renderRequest.getAttribute(ReqAttrKeys.PARAMS)}" />
	<c:choose>
		<c:when test="${data == null || empty(data)}">
			<liferay-ui:message key="no-data"/>
		</c:when>
		<c:otherwise>
			<c:choose>
				<c:when test="${data == null || empty (data)}">
					<liferay-ui:message key="noParams"/>
				</c:when>
				<c:otherwise>

					<portlet:actionURL name="updateParams" var="updateParamsURL" />
					<aui:form action="<%=updateParamsURL%>" method="POST">

						<table>

							<c:set var="typesMap" value="${renderRequest.getAttribute(ReqAttrKeys.UNIT_TYPES)}"/>

							<c:forEach var="displayParam" items="${data}">

								<c:set var="type" value="${typesMap[displayParam.unit]}"/>

								<tr>
									<td class="td-class">${displayParam.name}</td>
									<td class="td-class td-value">

										<c:set var="value" value="${displayParam.value}"/>
										<c:set var="unsupportedType" value="false" />

										<c:choose>

											<c:when test="${type == null}">
												${value}
											</c:when>

											<c:when test="${type.name == BOOLEAN_UNIT_TYPE}">
												<aui:input label="" name="value_${displayParam.id}" type="checkbox" checked="${value == 'true'}"/>
											</c:when>

											<c:when test="${type.name == INTEGER_UNIT_TYPE}">
												<aui:input cssClass="param-input" label="" name="value_${displayParam.id}" type="text" value="${value}">
													<aui:validator name="number" />
												</aui:input>
											</c:when>

											<c:when test="${type.name == STRING_UNIT_TYPE}">
												<aui:input cssClass="param-input" label="" name="value_${displayParam.id}" type="text" value="${value}" />
											</c:when>

											<c:otherwise>
												<c:set var="unsupportedType" value="true" />
												${value}
											</c:otherwise>
										</c:choose>
									</td>
									<td class="td-class td-unit">

										<c:set var="units" value="${displayParam.units}" />
										<c:set var="unitsLength" value="${fn:length(units)}" />

										<c:choose>

											<c:when test="${unitsLength == 0}">
												<liferay-ui:message key="unitsAreEmpty"></liferay-ui:message>
											</c:when>

											<c:otherwise>

												<c:choose>

													<c:when test="${unitsLength == 1}">
														${displayParam.unit}
													</c:when>

													<c:otherwise>
														<div class="unit-input-wrapper">
															<aui:select name="unit_${displayParam.id}" label="">
																<c:forEach var="unit" items="${units}">
																	<aui:option value="${unit}" selected='${displayParam.unit == unit}'>${unit}</aui:option>
																</c:forEach>
															</aui:select>
														</div>
													</c:otherwise>

												</c:choose>

												<c:if test="${type == null}">
													(<liferay-ui:message key="unitTypeNotFound"></liferay-ui:message>)
												</c:if>

											</c:otherwise>
										</c:choose>

										<c:if test="${unsupportedType}">
											(<liferay-ui:message key="unsupportedUnitType"></liferay-ui:message>: ${type.name})
										</c:if>

									</td>
								</tr>
							</c:forEach>

						</table>
						<aui:button type="submit" value="update"/>
					</aui:form>
					<br/>

				</c:otherwise>
			</c:choose>
		</c:otherwise>
	</c:choose>