### 1. Intro
Запущенный портлет доступен по ссылке http://178.252.83.114:8080/web/guest/pcab-display-params  

Login: **test@liferay.com**  
Password: **test**  

Права доступа ограничены для неавторизованных пользователей и для страницы, и для портлета (стандартными средствами Liferay).  

Заголовок портлета может быть изменен тоже стандартными средствами Liferay.

Настройки находятся в меню портлета "Настройки" (стандартный режим портлета для настроек - edit). Если не заданы параметры доступа к REST-сервису, используются по-умолчанию (http://178.252.83.114:8080/o/rest/pcab-display-params). В этом случае выводится предупреждение.  

### 2. Код
[REST-сервис] (/pcab-display-params/modules/rest) и [портлет] (/pcab-display-params/modules/portlet) реализованы в виде OSGI-модулей, могут быть запущенны на разных серверах и вообще могут существовать независимо друг от друга, ссылаясь на общую зависимость - [data-model] (/pcab-display-params/data-model) - модель данных, которая используется в обоих модулях.  

Т.к. в ТЗ ничего не сказано про источник данных, по-умолчанию используется [SampleDataProvider] (/pcab-display-params/modules/rest/src/main/java/ru/dartit/pcab/display_params/rest/data/SampleDataProvider.java). Для работы с реальными данными нужно реализовать интерфейс [DataProvider] (/pcab-display-params/modules/rest/src/main/java/ru/dartit/pcab/display_params/rest/data/DataProvider.java).

Есть небольшое отличие в схеме REST-запросов между ТЗ и текущей реализацией. В ТЗ используется обёртка - {"params": [params_array]}, в текущей реализации данные передаются без обёртки - [params_array]. Если формат из ТЗ критичен, можно исправить.

### 3. Тесты
Пока написан [тест только для REST-сервиса] (/pcab-display-params/modules/rest/src/test/java/RestServiceTest.java). Тест проверяет след сценарии:
 - получение параметров (getParams);
 - обновление параметров (update);
 - ошибку при попытке обновления с несуществующим id параметра.
 - получение ед. изм. (getUnits)
 
### 4. Сборка и запуск
Проект разрабатывается в Intellij IDEA с плагином для Liferay. Для сборки и запуска проекта достаточно установить плагин, импортировать проект, сделать initBundle и deploy modules ([документация] (https://dev.liferay.com/develop/tutorials/-/knowledge_base/7-0/intellij-idea))  
